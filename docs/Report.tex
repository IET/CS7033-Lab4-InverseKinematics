\documentclass[a4paper]{report} % V1.2
\usepackage[]{minted} 	%Code highlighting
\usepackage{gensymb}	%Degree symbol
\usepackage{listings}	%Alternate code listings
\usepackage{float}
\usepackage{caption}

%Adds a box around all your figures
%\floatstyle{boxed} 
%\restylefloat{figure}

\usepackage{graphicx}	%For includegraphics in figures

% \usepackage{tcolorbox}
% \usepackage{etoolbox}
% \BeforeBeginEnvironment{minted}{\begin{tcolorbox}}%
% \AfterEndEnvironment{minted}{\end{tcolorbox}}%
\lstset{language=c++, aboveskip=0pt}
\usepackage{etoolbox}

%\doi{10.1145/1559755.1559763}

% Copyright
%\setcopyright{acmcopyright}

\begin{document}

% Page heads

% Title portion
\title{CS7033\\ Real-Time Animation\\ Lab 4\\ Inverse Kinematics}


\author{
	%\alignauthor
		Daniel Walsh\\
	 	{Trinity College Dublin}\\
		{walshd15@tcd.ie}
}

\maketitle



\section{Introduction}

For this assignment I implemented the CCD algorithm to calculate new joint angles for a given bone chain. The inverse kinematics positions are re-calculated each frame and the bone transforms are updated with a new value. This allows the bones to be updated by the model's animation and then updated relative to this position by the CCD algorithm, as shown in Figure~\ref{fig:bob_reaching}.

Unfortunately due to time constraints and a lot of issues attempting to load the GV2 Peter model, I did not have time to implement any spline interpolation for this assignment.

\begin{figure}[ht]
\centering
\includegraphics[width=13cm]{bob_wide.PNG}
\caption{Bob Model With Right Hand Motion Driven By Base Animation and Left Hand Using CCD to Seek the Checkered Ball.}
\label{fig:bob_reaching}
\end{figure}

\section {Inverse Kinematics Calculations}
All of the IK calculations were performed in model space and the final rotation was then converted into the current bone's local space and once applied, all of the child bones were updated relative to this. the code used for the various spatial transforms is shown in Listing~\ref{listing:spatial_transforms}

\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}

//Transform rotational axis into local bone space
rotAxis = glm::vec3(  glm::inverse( modelMatrix * 
					targetBones[currLink]->finalTransformation  * 
						targetBones[currLink]->inverseBoneOffset) * 
							glm::vec4(rotAxis, 0.0f));

//Get bone's current position in world space
glm::vec3 currentPos = glm::vec3 ( modelMatrix * 
		glm::vec4(targetBones[currLink]->GetPosition(), 1.0f));

//Returns a bone's position in model space
glm::vec3 GetPosition()
{
	return glm::vec3( (finalTransformation * inverseBoneOffset)[3] );
}
\end{minted}
\caption{Transformations Between Local and Global Space Used in the Program}
\label{listing:spatial_transforms}
\end{listing}

The IK method takes in a string to identify a starting bone, the length of the bone chain, a target position, and the maximum number of CCD iterations to perform before giving up on finding a solution. This solution is a bit inflexible and given more time I'd like to be able to manually select the whole bone chain (probably by passing a vector of bones to the IK method) because at the moment, it is impossible to decide which child to visit to continue the bone chain so it is currently hard-coded to take the last bone in the vector of children so that I can start from the spine and move to the "upperarm.R" bone by default (which is actually the left arm of the model). Ideally it would be possible to choose to visit either the left arm, right arm, or neck. Listing~\ref{listing:ik_loop} shows an excerpt of the ikCCD method with the main loop of the CCD algorithm.

\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
while ( (distance = glm::distance(endEffectorPosition, target) > IK_THRESHOLD) 
			&& (iterations < maxIterations))
{	
	glm::vec3 currentPos = glm::vec3 ( modelMatrix * 
			glm::vec4(targetBones[currLink]->GetPosition(), 1.0f));

	glm::vec3 currentDir = glm::normalize(endEffectorPosition - currentPos);
	glm::vec3 targetDir	 = glm::normalize(target - currentPos);

	float cosAngle = glm::dot((currentDir), (targetDir));
	
\end{minted}
\caption{Iterative CCD Loop Used to Calculate Joint Angles for IK.}
\label{listing:ik_loop}
\end{listing}


\begin{listing}[H]\ContinuedFloat
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
	//Account for floating point errors (in practice this seems excessive)
	if (cosAngle > 1.0f || cosAngle < -1.0f)
	{
		if (cosAngle > 1.0f)
			cosAngle = 1.0f;

		if (cosAngle < -1.0f)
			cosAngle = -1.0f;
	}

	glm::vec3 rotAxis = (glm::cross(currentDir, targetDir));
	float angle = glm::acos(cosAngle);
	
	//Transform axis into bone space
	rotAxis = glm::vec3(  glm::inverse( modelMatrix *
		 targetBones[currLink]->finalTransformation  * 
		 	targetBones[currLink]->inverseBoneOffset) *
		 		 glm::vec4(rotAxis, 0.0f));

	//Renormalize rotation axis
	rotAxis = glm::normalize(rotAxis);
	
	glm::quat rotationQuaternion = glm::angleAxis(angle, rotAxis);
	glm::mat4 quaternionMatrix = glm::mat4_cast(rotationQuaternion);

	//Update Curent Bone
	targetBones[currLink]->rotation *= quaternionMatrix;
	targetBones[currLink]->Update();
	targetBones[currLink]->worldTransform = 
					targetBones[currLink]->parentTransform * 
						targetBones[currLink]->localTransform;
					
	targetBones[currLink]->finalTransformation = inverseTransform * 
			targetBones[currLink]->worldTransform * 
				targetBones[currLink]->boneOffset;

	//Propagate Update to Child Bones 
	for (int i = 0; i < targetBones[currLink]->children.size(); i ++)
		UpdateBones(targetBones[currLink]->children[i], targetBones[currLink]->
			worldTransform);

	//Find New End Effector
	if (targetBones[targetBones.size() -1]->children.size() > 0)
		endEffectorPosition = 
			targetBones[targetBones.size() -1]->children[0]->GetPosition();
	else
		endEffectorPosition = 
			targetBones[targetBones.size() -1]->GetPosition();

	endEffectorPosition = glm::vec3( modelMatrix * 
					glm::vec4(endEffectorPosition, 1.0f));

	currLink--;
	if (currLink < 0)
	{
		currLink = targetBones.size() -1;
		iterations++;
	}
}

\end{minted}
\caption{Iterative CCD Loop Used to Calculate Joint Angles for IK ( cont).}
\label{listing:ik_loop}
\end{listing}

Figure~\ref{figure:bob_debug} shows the program running in debug mode with examples of the model easily reaching a target and also trying and failing to reach the target.

\begin{figure}[H]
\minipage {0.45\textwidth}
\includegraphics[width=\linewidth]{bob}
\endminipage \hfill
\minipage {0.45\textwidth}
\includegraphics[width=\linewidth]{bob1}
\endminipage \hfill
\caption{Using CCD From Spine to Wrist to Reach Toward a Target. Blue Spheres are Locations Visited By Bones, the Red or Green Spheres are the Position of the End Effector Before and After the CCD Calculations (red means no solution found, green means end effector reached its target).}
\label{figure:bob_debug}
\end{figure}

\section{Combining Forward and Inverse Kinematics}

In order to combine both types of animation, the bone positions were updated based on the current time (as in the previous assignment) and then this was used as the starting position for the inverse kinematics algorithm as shown in Listing~\ref{listing:draw_functions}.

\begin{listing}[H]
\begin{minted}[frame=single,framesep=10pt, tabsize=2,fontsize=\footnotesize]{c++}
bob.UpdateAnimation(time);
bob.DrawIK(v, p, "spine", 4, ikTarget);
\end{minted}
\caption{Calls needed to apply inverse kinematics to an animated model.}
\label{listing:draw_functions}
\end{listing}

% Bibliography
%\bibliographystyle{abbrv}
%\bibliography{references}
\end{document}
