#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texCoordIn;

uniform mat4 modelMat;
uniform mat4 viewMat;
uniform mat4 projectionMat;

out VS_OUT {
	vec3 WorldPos;
	vec3 Normal;
	vec2 TexCoords;
	vec3 TangentLightPos;
	vec3 TangentViewPos;
	vec3 TangentFragPos;
} vs_out;

void main()
{
	gl_Position = projectionMat * viewMat * modelMat * vec4(position.xyz, 1.0);
	// We swap the y-axis by substracting our coordinates from 1. This is done because most images have the top y-axis inversed with OpenGL's top y-axis.
	vs_out.TexCoords = texCoordIn;
	//TexCoord = vec2(texCoordIn.x, 1.0 - texCoordIn.y);
}