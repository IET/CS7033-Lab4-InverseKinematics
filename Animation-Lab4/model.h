//Courtesy of http://learnopengl.com/
#pragma once
// Std. Includes
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
//using namespace std;
// GL Includes
#include <GL/glew.h> // Contains all the necessary OpenGL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtx/norm.hpp>
#include <IL/il.h>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "mesh.h"
#include "Shader.hpp"

const std::string DEFAULT_TEXTURE_DIRECTORY = "../textures/";

GLint TextureFromFile(const char* path, string directory, bool gamma = false);

class Model 
{
public:
	glm::vec3 location;
	glm::quat quaternionOrientaton;

	glm::mat4 modelMatrix;

	/*  Model Data */
	std::vector<Texture> textures_loaded;	// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
	std::vector<Mesh> meshes;

	string directory;
	bool gammaCorrection;

	glm::vec3 minBounds;
	glm::vec3 maxBounds;

	/*  Functions   */
	//Default constructor
	Model();

	// Constructor, expects a filepath to a 3D model.
	Model(string const & path, bool gamma = false);

	//Copy Constructor
	Model::Model(const Model& other);
	Model operator=(const Model &other );

	//Inverse Kinematics Calculations
	void ikCCD(glm::vec4 target);
	void ikCCD(const std::string &boneName, int linkLength, const glm::vec3 &target, int maxIterations);

	// Draws the model, and thus all its meshes
	void Draw(glm::mat4 &v, glm::mat4 &p);
	void Draw(glm::mat4 &v, glm::mat4 &p, float time);
	void DrawIK(glm::mat4 &v, glm::mat4 &p, std::string ikRootBoneName, int ikLinkLength, glm::vec3 target);

	//Debug Drawing Functions
	void DrawJoints(glm::mat4 &v, glm::mat4 &p);
	void DrawBones(glm::mat4 &v, glm::mat4 &p);
	
	//Skeletal animation
	void BoneTransform(float time, vector<glm::mat4> &transforms);

	void UpdateAnimation(float time);

	void CalcInterpolatedPosition(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void CalcInterpolatedRotation(aiQuaternion& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
	void CalcInterpolatedScaling(aiVector3D& Out, float AnimationTime, const aiNodeAnim* pNodeAnim);
		
	unsigned int FindPosition(float AnimationTime, const aiNodeAnim* pNodeAnim);
	unsigned int FindRotation(float AnimationTime, const aiNodeAnim* pNodeAnim);
	unsigned int FindScaling(float AnimationTime, const aiNodeAnim* pNodeAnim);

	//Getters
	Shader* getShader();
	glm::mat4 getModelMatrix();
	glm::vec3 getLocation();

	//Setters
	void setShader(Shader* s);
	void setLocation (glm::vec3 newPos);
	
	//Geometric Transformations
	void moveBy(glm::vec3 move);
	void setRotation(glm::vec3 newRot);
	void rotateBy(glm::vec3 rot);
	void translate(glm::vec3 t);
	void scale(glm::vec3 s);
	
	void load(string path);
	
	~Model();
private:
	unsigned int numBones;
	std::vector<Bone> allBones; //m_BoneInfo in OGLDEV code
	map<string,unsigned int> boneMap; // maps a bone name to its index
	unsigned int numVerts;
	Shader* shader;
	const aiScene* scene;
	glm::mat4 inverseTransform;
	Assimp::Importer importer;
	//std::string path;
	std::vector<aiAnimation*> animations;
	Bone *rootBone;

	void ikCCD(Bone *b, glm::vec4 target);
	glm::vec4 findEndEffectorPosition(Bone *b);

	/*  Functions   */
	// Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes std::vector.
	void loadModel(string path);

	// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
	void processNode(aiNode* node, const aiScene* scene);

	Mesh processMesh(aiMesh* mesh, const aiScene* scene);

	void DrawBones(glm::mat4 &v, glm::mat4 &p, Bone* bone);

	void BuildBoneHierarchy(aiNode* node);
	void ReadBoneHeirarchy(float AnimationTime, Bone* bone, const glm::mat4 ParentTransform);
	const aiNodeAnim* FindNodeAnim(const aiAnimation* pAnimation, const string NodeName);
	void UpdateBones(Bone* bone, glm::mat4 ParentTransform);
	void TransformBones(Bone* bone);

	// Checks all material textures of a given type and loads the textures if they're not loaded yet.
	// The required info is returned as a Texture struct.
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName);

	static GLint TextureFromFile(const char* path, std::string directory, bool gamma=false);
};
