#ifndef SHADER_HPP
#define SHADER_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

#include <GL/glew.h>
#include <glm/mat4x4.hpp>

class Shader
{
public:
	Shader();

	void enableShader();
	void disableShader();

	void setUniformMatrix4fv(std::string uniformName, glm::mat4x4 matrix);
	
	void setUniform1i(std::string uniformName, int value);

	void setUniform1f(std::string uniformName, float value);
	void setUniform4f(std::string uniformName, float x, float y, float z, float w);

	void setUniformVector3fv(std::string uniformName, glm::vec3 vec);
	void setUniformVector4fv(std::string uniformName, glm::vec4 v);
	

	bool loadShadersFromFiles(std::string vertShaderPath, std::string fragShaderPath);

protected:
	std::string _readShaderFile(std::string path);
	bool _compileShader(std::string source, GLuint shaderType, GLuint shaderID);
	bool _linkShaders(GLuint shaderProgram, GLuint vertShader, GLuint fragShader);

	GLuint _getUniformLocation(std::string uniform);

	GLuint _vertShaderID;
	GLuint _fragShaderID;
	GLuint _shaderProgramID;

	std::map<std::string, GLuint> _shaderVariableLocations;
};

#endif