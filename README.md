# Assignment 4 of Real-Time Animation Module For TCD IET 2016

Implementation of the cyclic coordinate descent algorithm for inverse kinematics calculations.

In this program a character (Bob) will play his normal forward kinematics animation while also reaching out with his left arm to reach the checkered ball.

### Controls
WASD + mouse to control camera

Arrow keys to move target (shift + up/down to move in z-axis)

### Links
Built binaries available [here](https://www.dropbox.com/home/iet/cs7033-real-time%20animation/labs/Lab4/bin).

Video [here](https://youtu.be/107eLVcNvcM).